# vanilla

A vanilla javascript web application.

### Docker Development

```bash
# run container
docker run --rm -it \
    -v $(pwd)/package.json:/project/package.json \
    -v $(pwd)/src:/project/src \
    -v $(pwd)/webpack.common.js:/project/webpack.common.js \
    -v $(pwd)/webpack.dev.js:/project/webpack.dev.js \
    -w /project \
    -p 8080:8080 \
    node \
    bash

# install dependencies
npm install

# run application
npm run dev --port 8080 --host 0.0.0.0 --allowed-hosts all
```

## env values

* HOST
* PORT
* APP_NAME