import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import HtmlBundlerPlugin from "html-bundler-webpack-plugin";
import webpack from "webpack";

const __dirname = dirname(fileURLToPath(import.meta.url));

const commonConfig = {

    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["css-loader"]
            },
            {
                test: /\.(ico|png|jp?g|svg)/,
                type: "asset",
                generator: { filename: "img/[name].[hash:8][ext]" },
                parser: { dataUrlCondition: { maxSize: 2 * 1024 } }
            }
        ]
    },

    output: {
        path: path.resolve(__dirname, "dist"),
        clean: true
    },

    plugins: [
        new HtmlBundlerPlugin({
            entry: { index: "./src/index.html" },
            js: { filename: "[name].[contenthash:8].js" },
            css: { filename: "[name].[contenthash:8].css" }
        }),
        new webpack.DefinePlugin({
            "process.env": {
                "APP_NAME": JSON.stringify(process.env.APP_NAME),
            }
        })
    ],

    resolve: {
        alias: {
          "@scripts": path.join(__dirname, "src/modules"),
          "@styles": path.join(__dirname, "src/style"),
          "@images": path.join(__dirname, "src/assets"),
        },
      }

 };

 export { commonConfig };
 export default commonConfig;